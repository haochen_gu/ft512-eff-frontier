#include "asset.hpp"

#include <iostream>
#include <cstdlib>
#include <string>
#include <vector>
#include <fstream>
#include <algorithm>
#include <sstream>
#include <cmath>
#include <iomanip>
#include <string.h>
#include <cstring>
#include <Eigen/Dense>

using namespace Eigen;

void checkNum(std::string input) {

  int count = 0;
  // check each letter if each are numbers
  for (size_t i=0; i<input.length(); i++) {
    if ((std::isdigit(input[i]) == 0) && (input[i] != '.') && (input[i] != '-') && (input[i] != 'e')) {
      std::cerr << "Invalid data, letters contained.\n";
      std::cout << input << std::endl;
      exit(EXIT_FAILURE);
    }
    
    if (input[i] == '.') {
      count++;
      if (count > 1) {
	std::cerr << "Invalid data, double dots.\n";
	exit(EXIT_FAILURE);
      }
    }
      
  }
}


int numAsset(std::string filename) {
  std::ifstream asset_file (filename.c_str(), std::ifstream::in);
  int num = 0;
  std::string line;
  while (std::getline(asset_file, line)) {
    num++;
  }
  return num;
}

// read the data of asset and store them 
std::vector<Asset> readAsset(std::string filename) {

  std::ifstream asset_file (filename.c_str(), std::ifstream::in);

  if (!asset_file.is_open()) {
    std::cerr << "Invalid input of universe file.\n";
    exit(EXIT_FAILURE);
  }

  std::vector<Asset> A_data;
  std::string line;
  std::string delim = ",";
  
  while (std::getline(asset_file, line)) {
    size_t p1;
    size_t p2 = 0;
    Asset temp_asset;
    int count = 0;
    
    while ((p1 = line.find_first_not_of(delim, p2)) != std::string::npos) {
      count++;
      p2 = line.find(delim, p1);
      std::string segment = line.substr(p1, p2-p1);
      if (count==1) {
	temp_asset.name = segment;
      }
      else if (count==2) {
	checkNum(segment);
	temp_asset.ror = atof(segment.c_str());
      }
      else if (count==3) {
	checkNum(segment);
	temp_asset.std = atof(segment.c_str());
      }
      else {
	std::cerr << "Invalid input of universe file.\n";
	exit(EXIT_FAILURE);
      }
    }
    if (count != 3) {
      std::cerr << "Invalid data, wrong universe data format.\n";
      exit(EXIT_FAILURE);
    }
    A_data.push_back(temp_asset);
  }
  
  return A_data;
}

// read the data of correlation file and store them
std::vector<std::vector<double> > readCorr(std::string filename) {

  std::ifstream corr_file (filename.c_str(), std::ifstream::in);

  if (!corr_file.is_open()) {
    std::cerr << "Invalid input of correlation file.\n";
    exit(EXIT_FAILURE);
  }
  std::vector <std::vector<double> > data_corr;
  std::string line;
  std::string delim = ",";

  int row = numAsset(filename);
  
  while (std::getline(corr_file, line)) {
    int col = 0;
    size_t p1;
    size_t p2 = 0;
    std::vector <double> data_row;
    while ((p1 = line.find_first_not_of(delim, p2)) != std::string::npos) {
      col++;
      p2 = line.find(delim, p1);
      std::string segment = line.substr(p1, p2-p1);
      checkNum(segment);
      data_row.push_back(atof(segment.c_str()));
    }

    if (row != col) {
      std::cerr << "Invalid input, wrong format of correlation file..\n";
      exit(EXIT_FAILURE);
    }
    
    data_corr.push_back(data_row);
  }
  return data_corr;
}

// calculate the covariance matrix 
MatrixXd create_cov_matrix(int num_asset, std::vector<Asset> data_A, std::vector <std::vector<double> > data_corr) {
  MatrixXd cov(num_asset, num_asset);
  for (int i=0; i<num_asset; i++) {
    for (int j=0; j<num_asset; j++) {
      cov(i, j) = data_A[i].std * data_A[j].std * data_corr[i][j];
    }
  }
  return cov;
}

// calculate the overall variance of each weight
double calculate_var(VectorXd optimal, std::vector <std::vector<double> > data_corr, int num_asset, std::vector<Asset> data_A) {
  double sum = 0;
  for (int i=0; i<num_asset; i++) {
    for (int j=0; j<num_asset; j++){
      double var = optimal(i) * optimal(j) * data_corr[i][j] * data_A[i].std * data_A[j].std;
      sum = sum + var;
    }
  }
  double std = std::sqrt(sum);
  return std;
}

