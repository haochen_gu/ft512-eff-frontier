#include "asset.hpp"
#include "parse.cpp"

#include <iostream>
#include <cstdlib>
#include <string>
#include <vector>
#include <fstream>
#include <algorithm>
#include <sstream>
#include <cmath>
#include <iomanip>
#include <string.h>
#include <cstring>
#include <Eigen/Dense>

using namespace Eigen;

// calculate and print for the unrestricted cases
void shortsell(std::vector<Asset> data_A, std::vector <std::vector<double> > data_corr, int num_asset, MatrixXd covM) {

  MatrixXd A(2, num_asset);

  // fill A
  for (int i=0; i<2; i++) {
    for (int j=0; j<num_asset; j++) {
      if (i==0) {
        A(i, j) = 1;
      }
      else {
        A(i, j) = data_A[j].ror;
      }
    }
  }

  MatrixXd zero = MatrixXd::Zero(2, 2);

  // fill the big matrix
  MatrixXd X(num_asset+2, num_asset+2);
  X << covM, A.transpose(), A, zero;

  std::cout << "ROR,volatility" << std::endl;

  // handle with different ror cases and print 
  for (int i=1; i<=26; i++) {
    MatrixXd B = MatrixXd::Zero(num_asset+2, 1);
    double adjusted_ror = 0.01 * i;
    B(num_asset) = 1;
    B(num_asset+1) = adjusted_ror;
    
    VectorXd W(num_asset+2);
    W = X.fullPivHouseholderQr().solve(B);
    VectorXd optimal = W.head(num_asset);

    // calculate the minimum volitality
    double vol = calculate_var(optimal, data_corr, num_asset, data_A);

    std::cout << std::fixed << std::setprecision(1) << adjusted_ror*100 << "%" << ",";
    std::cout << std::fixed << std::setprecision(2) << vol*100 << "%" << std::endl;

  }
}

// calculate the for the restricted case
double no_short_vol(std::vector<Asset> data_A, std::vector <std::vector<double> > data_corr, int num_asset, MatrixXd covM, double rate) {

  // first calculate the unrestricted cases
  MatrixXd A(2, num_asset);

  for (int i=0; i<2; i++) {
    for (int j=0; j<num_asset; j++) {
      if (i==0) {
	A(i, j) = 1;
      }
      else {
	A(i, j) = data_A[j].ror;
      }
    }
  }

  MatrixXd zero = MatrixXd::Zero(2, 2);

  MatrixXd X(num_asset+2, num_asset+2);
  X << covM, A.transpose(), A, zero;

  MatrixXd B = MatrixXd::Zero(num_asset+2, 1);
  B(num_asset) = 1;
  B(num_asset+1) = rate;
  
  VectorXd W(num_asset+2);
  W = X.fullPivHouseholderQr().solve(B);
  
  MatrixXd new_A = A;
  VectorXd W2 = W;

  // start to set negative weight into 0 and stop till no negative weight exist 
  for (int i=0;;i++) {

    // identification for existance of negative weight 
    bool neg_iden = false;
    
    for (int j=0; j<num_asset; j++) {
      // add row to A, so that we can set negative weight to 0
      if (W2(j)<0) {
	MatrixXd temp = new_A;
	new_A.resize(temp.rows()+1, num_asset);
	MatrixXd zero2 = MatrixXd::Zero(1, num_asset);
	zero2(j) = 1;
	new_A << temp, zero2;
	neg_iden = true;
      }
    }

    // stop statement
    if (neg_iden == false) {
      break;
    }

    // make the new big matrix
    MatrixXd X2(num_asset+new_A.rows(), num_asset+new_A.rows());
    MatrixXd zero3 = MatrixXd::Zero(new_A.rows(), new_A.rows());
    X2 << covM, new_A.transpose(), new_A, zero3;
    
    MatrixXd B2 = MatrixXd::Zero(num_asset+new_A.rows(), 1);
    B2(num_asset) = 1;
    B2(num_asset+1) = rate;

    // calculate the new optimal weight 
    W2.resize(num_asset+new_A.rows());
    W2 = X2.fullPivHouseholderQr().solve(B2);
  }
  
  double vol = calculate_var(W2, data_corr, num_asset, data_A);
  return vol;
}

// print each ror case
void printNoShort(std::vector<Asset> data_A, std::vector <std::vector<double> > data_corr, int num_asset, MatrixXd covM) {
  std::cout << "ROR,volatility" << std::endl;
  for (int i=1; i<=26; i++) {
    double vol = no_short_vol(data_A, data_corr, num_asset, covM, i*0.01);
    std::cout << std::fixed << std::setprecision(1) << i*0.01*100 << "%" << ",";
    std::cout << std::fixed << std::setprecision(2) << vol*100 << "%" << std::endl;
  }
}


int main(int argc, char ** argv) {

  if (argc != 3 && argc != 4) {
    std::cerr << "Invalid input of command line\n";
    return EXIT_FAILURE;
  }

  // unrestricted, can short sell
  if (argc == 3) {
    // read data
    std::vector<Asset> data_A = readAsset(argv[1]);
    std::vector <std::vector<double> > data_corr = readCorr(argv[2]);
    int num_asset = numAsset(argv[1]);
    // calculate the volatility
    MatrixXd covM = create_cov_matrix(num_asset, data_A, data_corr);
    shortsell(data_A, data_corr, num_asset, covM);
  }

  // restricted, cannot short sell 
  if (argc == 4) {

    std::vector<Asset> data_A;
    std::vector <std::vector<double> > data_corr;
    int num_asset;
    bool trigger = false;
    
    std::string check1 = argv[1];
    if (check1 == "-r") {
      data_A = readAsset(argv[2]);
      data_corr = readCorr(argv[3]);
      num_asset = numAsset(argv[2]);
      trigger = true;
    }

    std::string check2 = argv[2];
    if (check2 == "-r") {
      data_A = readAsset(argv[1]);
      data_corr = readCorr(argv[3]);
      num_asset = numAsset(argv[1]);
      trigger = true;
    }
    
    std::string check3 = argv[3];
    if (check3 == "-r") {
      data_A = readAsset(argv[1]);
      data_corr = readCorr(argv[2]);
      num_asset = numAsset(argv[1]);
      trigger = true;
    }

    if (trigger != true) {
      std::cerr << "Invalid trigger of no short sell case.\n";
      return EXIT_FAILURE;
    }

    MatrixXd covM = create_cov_matrix(num_asset, data_A, data_corr);
    printNoShort(data_A, data_corr, num_asset, covM);
  }  
}
