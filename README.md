# Planning for calculation of sharp ratio

|                         Asset                                | Estimated Time |
| Variavle -------------------------------------------------------------------  |
|                      double avgReturn                        |    30 seconds  | 
|                      double std                              |    30 seconds  |
|                      double weight                           |    30 seconds  |
|                      String dates                            |    30 seconds  |
| Class ----------------------------------------------------------------------- |
|                      calculateAvgReturn()                    |    15 mins     |
|                      calculateStd()                          |    15 mins     |

---------------------------------------------------------------------------------
---------------------------------------------------------------------------------

|             Portfolio Class              |            Estimated Time          |
| Variavle -------------------------------------------------------------------  |
|         ArrayList<Asset> assetList       |              30 seconds            |
|         double porfolio_return           |              30 seconds            |
|         double porfolio_std              |              30 seconds            | 
|         corr_matrix                      |              30 seconds            |'
| Class ----------------------------------------------------------------------- |
|         get_corr_matrix()                |              30 minutes            |
|         getReturns()                     |              40 minutes            |
|         getStd()                         |              40 minutes            |


---------------------------------------------------------------------------------
---------------------------------------------------------------------------------

|       Main          |  Estimated Time  |
|    readData()       |     30 mins      |
|    restricted()     |     4 hours      |
|    unrestricted()   |     2 hours      |
|    cal_sharp_r()    |     1 hour       |




## 
